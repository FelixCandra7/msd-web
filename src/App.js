import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import moment from 'moment-timezone';
import {
  BrowserRouter as Router, Route, Switch,
} from 'react-router-dom';
import reducer from './redux/reducer';
import * as Constant from './constant';
import { PrivateRoute } from './component';
import {
  SplashPage, LoginPage, HomePage, ProfilePage, LogoutPage, ForgetPasswordPage, RegistrationPage,
} from './page';

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['images', 'uiLogin', 'uiProfile', 'uiForgetPassword', 'form'],
};

const persistedReducer = persistReducer(persistConfig, reducer);

const IS_PRODUCTION = false;
const store = IS_PRODUCTION
  ? createStore(persistedReducer, applyMiddleware(thunk))
  : createStore(persistedReducer, applyMiddleware(thunk, logger));
const persistor = persistStore(store);

moment.tz.setDefault('UTC');

export default () => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <div>
          <Switch>
            <PrivateRoute path={Constant.ROUTE_NAME_HOME} component={HomePage} />
            <PrivateRoute path={Constant.ROUTE_NAME_PROFILE} component={ProfilePage} />
            <PrivateRoute path={Constant.ROUTE_NAME_LOGOUT} component={LogoutPage} />
            <Route path={Constant.ROUTE_NAME_LOGIN} component={LoginPage} />
            <Route path={Constant.ROUTE_NAME_FORGET_PASSWORD} component={ForgetPasswordPage} />
            <Route path={Constant.ROUTE_NAME_REGISTER_EMAIL} component={RegistrationPage} />
            <Route path="/" component={SplashPage} />
          </Switch>
        </div>
      </Router>
    </PersistGate>
  </Provider>
);
