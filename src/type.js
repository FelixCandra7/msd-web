import PropTypes from 'prop-types';

export const NavigationShape = PropTypes.shape({
  navigation: PropTypes.func,
});

export const ProfileShape = PropTypes.shape({
  id: PropTypes.string,
  username: PropTypes.string,
  fullName: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string,
  jobTitle: PropTypes.string,
  profilePicture: PropTypes.string,
});
