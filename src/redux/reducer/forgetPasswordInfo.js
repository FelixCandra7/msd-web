import * as Action from '../action';

const forgetPasswordInfoInitialState = {
  email: undefined,
  pinExpirationDate: undefined,
  pin: undefined,
  password: undefined,
};

export default (state = forgetPasswordInfoInitialState, action) => {
  switch (action.type) {
    case Action.ADD_FORGET_PASSWORD_INFO:
      return { ...state, ...action.info };
    case Action.REMOVE_FORGET_PASSWORD_INFO:
      return forgetPasswordInfoInitialState;
    default: return state;
  }
};
