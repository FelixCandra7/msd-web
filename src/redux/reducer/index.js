import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import * as Action from '../action';
import authentication from './authentication';
import images from './images';
import users from './users';
import currentUser from './currentUser';
import forgetPasswordInfo from './forgetPasswordInfo';
import registrationInfo from './registrationInfo';
import uiLogin from './uiLogin';
import uiProfile from './uiProfile';
import uiForgetPassword from './uiForgetPassword';
import uiRegistration from './uiRegistration';

const reducer = combineReducers({
  authentication,
  currentUser,
  users,
  images,
  forgetPasswordInfo,
  registrationInfo,
  uiLogin,
  uiProfile,
  uiForgetPassword,
  uiRegistration,
  form: formReducer,
});

export default (state, action) => {
  const matches = /(SET|CLEAR)_ERROR_(.+)/.exec(action.type);
  if (matches) {
    const reduxState = reducer(state, action);
    const [, stateOperation, stateName] = matches;
    const stateSlice = reduxState[stateName];
    if (stateSlice && stateOperation === 'SET') {
      return {
        ...reduxState,
        [stateName]: { ...stateSlice, error: action.error },
      };
    }
    if (stateSlice && stateOperation === 'CLEAR') {
      return {
        ...reduxState,
        [stateName]: { ...stateSlice, error: undefined },
      };
    }
  }

  if (action.type === Action.CLEAR_ALL_STATES) {
    return reducer(undefined, action);
  }
  return reducer(state, action);
};
