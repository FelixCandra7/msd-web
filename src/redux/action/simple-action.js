
export const ADD_TOKEN = 'ADD_TOKEN';
export const REMOVE_TOKEN = 'REMOVE_TOKEN';

export const LOGGING_IN = 'LOGGING_IN';
export const LOGGING_OUT = 'LOGGING_OUT';

export const CLEAR_ALL_STATES = 'CLEAR_ALL_STATES';

export const ADD_USER = 'ADD_USER';
export const REMOVE_USER = 'REMOVE_USER';
export const ADD_PROFILE_PICTURE = 'ADD_PROFILE_PICTURE';
export const REMOVE_PROFILE_PICTURE = 'REMOVE_PROFILE_PICTURE';

export const ADD_CURRENT_USER = 'ADD_CURRENT_USER';

export const UPLOADING_PROFILE_DETAILS = 'UPLOADING_PROFILE_DETAILS';
export const UPLOADING_PROFILE_PICTURE = 'UPLOADING_PROFILE_PICTURE';
export const DOWNLOADING_PROFILE_PICTURE = 'DOWNLOADING_PROFILE_PICTURE';
export const REMOVING_PROFILE_PICTURE = 'REMOVING_PROFILE_PICTURE';
export const UPDATING_PASSWORD = 'UPDATING_PASSWORD';

export const SET_PROFILE_SCREEN_EDIT_MODE = 'SET_PROFILE_SCREEN_EDIT_MODE';

export const ADD_IMAGE = 'ADD_IMAGE';
export const REMOVE_IMAGE = 'REMOVE_IMAGE';

export const SUBMITTING_FORGET_PASSWORD = 'SUBMITTING_FORGET_PASSWORD';
export const SUBMITTING_RESET_PASSWORD = 'SUBMITTING_RESET_PASSWORD';
export const SUBMITTING_EMAIL_REGISTRATION = 'SUBMITTING_EMAIL_REGISTRATION';
export const SUBMITTING_REGISTRATION_INFO = 'SUBMITTING_REGISTRATION_INFO';

export const ADD_FORGET_PASSWORD_INFO = 'ADD_FORGET_PASSWORD_INFO';
export const REMOVE_FORGET_PASSWORD_INFO = 'REMOVE_FORGET_PASSWORD_INFO';

export const ADD_REGISTRATION_INFO = 'ADD_REGISTRATION_INFO';
export const REMOVE_REGISTRATION_INFO = 'REMOVE_REGISTRATION_INFO';

export const setUIError = (stateName, error) => ({
  type: `SET_ERROR_${stateName}`,
  error,
});

export const clearUIError = stateName => ({
  type: `CLEAR_ERROR_${stateName}`,
});

export const submittingRegistrationInfo = status => ({
  type: SUBMITTING_REGISTRATION_INFO,
  status,
});

export const addRegistrationInfo = info => ({
  type: ADD_REGISTRATION_INFO,
  info,
});

export const removeRegistrationInfo = () => ({
  type: REMOVE_REGISTRATION_INFO,
});

export const submittingEmailRegistration = status => ({
  type: SUBMITTING_EMAIL_REGISTRATION,
  status,
});

export const removeForgetPasswordInfo = () => ({
  type: REMOVE_FORGET_PASSWORD_INFO,
});

export const addForgetPasswordInfo = info => ({
  type: ADD_FORGET_PASSWORD_INFO,
  info,
});

export const submittingResetPassword = status => ({
  type: SUBMITTING_RESET_PASSWORD,
  status,
});

export const submittingForgetPassword = status => ({
  type: SUBMITTING_FORGET_PASSWORD,
  status,
});

export const addImage = image => ({
  type: ADD_IMAGE,
  image,
});

export const removeImage = id => ({
  type: REMOVE_IMAGE,
  id,
});

export const uploadingProfilePicture = status => ({
  type: UPLOADING_PROFILE_PICTURE,
  status,
});

export const uploadingProfileDetails = status => ({
  type: UPLOADING_PROFILE_DETAILS,
  status,
});

export const downloadingProfilePicture = status => ({
  type: DOWNLOADING_PROFILE_PICTURE,
  status,
});

export const removingProfilePicture = status => ({
  type: REMOVING_PROFILE_PICTURE,
  status,
});

export const updatingPassword = status => ({
  type: UPDATING_PASSWORD,
  status,
});

export const setProfileScreenEditMode = status => ({
  type: SET_PROFILE_SCREEN_EDIT_MODE,
  status,
});

export const addCurrentUser = user => ({
  type: ADD_CURRENT_USER,
  user,
});

export const addUser = user => ({
  type: ADD_USER,
  user,
});

export const removeUser = id => ({
  type: REMOVE_USER,
  id,
});

export const addProfilePicture = (userId, imageId) => ({
  type: ADD_PROFILE_PICTURE,
  userId,
  imageId,
});

export const removeProfilePicture = userId => ({
  type: REMOVE_PROFILE_PICTURE,
  userId,
});

export const addToken = token => ({
  type: ADD_TOKEN,
  token,
});

export const removeToken = () => ({
  type: REMOVE_TOKEN,
});

export const loggingIn = status => ({ type: LOGGING_IN, status });
export const loggingOut = status => ({ type: LOGGING_OUT, status });

export const clearAllStates = () => ({
  type: CLEAR_ALL_STATES,
});
