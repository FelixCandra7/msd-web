import { addUser, addCurrentUser } from '../simple-action';
import * as Helper from '../../../helper';

export default () => async (dispatch, getState) => {
  const { token } = getState().authentication;
  const profile = await Helper.downloadMyProfile(token);

  if (profile) {
    const user = {
      id: profile.id,
      username: profile.username,
      fullName: profile.fullName,
    };
    dispatch(addUser(user));

    const currentUser = {
      id: profile.id,
      email: profile.email,
      phone: profile.phone,
      jobTitle: profile.jobTitle,
    };
    dispatch(addCurrentUser(currentUser));
  }
};
