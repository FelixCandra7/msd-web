import { loggingIn, addToken } from '../simple-action';
import downloadMyProfileAsync from './downloadMyProfileAsync';
import downloadProfilePictureAsync from './downloadProfilePictureAsync';
import * as Helper from '../../../helper';
import LocalizedString from '../../../localization';
import * as Constant from '../../../constant';

export default (username, password, navigateTo) => async (dispatch, getState) => {
  if (username === '' || password === '') {
    throw new Error(LocalizedString.loginScreen.errEmptyUsernamePassword);
  }

  try {
    dispatch(loggingIn(true));
    const auth = await Helper.login(username, password);
    dispatch(addToken(auth.token));

    await dispatch(downloadMyProfileAsync());

    const { id } = getState().currentUser;
    await dispatch(downloadProfilePictureAsync(id));

    navigateTo(Constant.ROUTE_NAME_HOME);
  } finally {
    dispatch(loggingIn(false));
  }
};
