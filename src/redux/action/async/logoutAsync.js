import { loggingOut, clearAllStates } from '../simple-action';
import * as Helper from '../../../helper';
import * as Constant from '../../../constant';

export default navigateTo => async (dispatch, getState) => {
  try {
    dispatch(loggingOut(true));
    const { token } = getState().authentication;
    await Helper.logout(token);

    navigateTo(Constant.ROUTE_NAME_LOGIN);
  } finally {
    await Helper.removeAllStorage();
    dispatch(clearAllStates());
    dispatch(loggingOut(false));
  }
};
