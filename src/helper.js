import React from 'react';
import {
  osName, osVersion, mobileModel, mobileVendor,
  browserName, fullBrowserVersion,
} from 'react-device-detect';
import uuid from 'uuid/v4';
import _ from 'lodash';
import {
  FormControl, InputLabel, Input, FormHelperText,
} from '@material-ui/core';
import * as Constant from './constant';

const getHttpHeaders = (authenticationToken) => {
  let headers = {
    'Content-Type': Constant.HTTP_HEADER_VALUE_JSON,
    'X-DeviceId': 'WebApp',
    'X-DeviceManufacturer': mobileVendor,
    'X-DeviceModel': mobileModel,
    'X-OSName': osName,
    'X-OSVersion': osVersion,
    'X-AppVersion': 'WebApp',
    'X-Notes': `Browser: ${browserName} v${fullBrowserVersion}`,
  };

  if (authenticationToken) {
    headers = { ...headers, Authorization: authenticationToken };
  }

  return headers;
};

const sendGetRequest = async (apiPath, authenticationToken) => {
  const url = `${Constant.REST_BASE_URL}${apiPath}`;
  const method = Constant.REST_METHOD_GET;
  const headers = getHttpHeaders(authenticationToken);
  const response = await fetch(url, { method, headers });
  const responseText = await response.text();
  if (response.status >= 200 && response.status <= 299) {
    if (responseText) {
      return JSON.parse(responseText);
    }
    return undefined;
  }
  throw new Error(responseText);
};

export const convertBlobToBase64 = blob => new Promise((resolve, reject) => {
  const reader = new FileReader();
  reader.onerror = reject;
  reader.onload = () => {
    const pattern = /data[:][^/]+\/[^;]+;base64,/gi;
    resolve(reader.result.replace(pattern, ''));
  };
  reader.readAsDataURL(blob);
});

const convertBase64ToBlob = (b64Data, contentType = '', sliceSize = 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i += 1) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, { type: contentType });
  return blob;
};


const sendGetImageRequest = async (apiPath, authenticationToken) => {
  const url = `${Constant.REST_BASE_URL}${apiPath}`;
  const method = Constant.REST_METHOD_GET;
  const headers = getHttpHeaders(authenticationToken);
  const response = await fetch(url, { method, headers });
  if (response.status >= 200 && response.status <= 299) {
    const blob = await response.blob();
    return blob.size === 0 ? undefined : convertBlobToBase64(blob);
  }
  const responseText = await response.text();
  throw new Error(responseText);
};

const sendPostRequest = async (apiPath, body, authenticationToken) => {
  const bodyStr = JSON.stringify(body);
  const url = `${Constant.REST_BASE_URL}${apiPath}`;
  const method = Constant.REST_METHOD_POST;
  const headers = getHttpHeaders(authenticationToken);
  const response = await fetch(url, { method, headers, body: bodyStr });
  const responseText = await response.text();
  if (response.status >= 200 && response.status <= 299) {
    if (responseText) {
      return JSON.parse(responseText);
    }
    return undefined;
  }
  throw new Error(responseText);
};

const getMimeType = (dataURI) => {
  const suffix = ';base64';
  const prefix = 'data:';
  const idx = dataURI.indexOf(suffix);

  if (idx > 0) {
    return dataURI.substring(0, idx).replace(prefix, '');
  }
  return '';
};

const sendPostImageRequest = async (apiPath, image, authenticationToken) => {
  const url = `${Constant.REST_BASE_URL}${apiPath}`;
  const method = Constant.REST_METHOD_POST;
  const headers = getHttpHeaders(authenticationToken);

  const mimeType = getMimeType(image);
  const blob = convertBase64ToBlob(image, mimeType);
  const body = new FormData();
  body.append('file', blob);

  const request = {
    method,
    headers: _.omit(headers, 'Content-Type'),
    body,
  };

  const response = await fetch(url, request);

  const responseText = await response.text();
  if (response.status >= 200 && response.status <= 299) {
    if (responseText) {
      return JSON.parse(responseText);
    }
    return undefined;
  }

  throw new Error(responseText);
};

const sendDeleteRequest = async (apiPath, authenticationToken) => {
  const url = `${Constant.REST_BASE_URL}${apiPath}`;
  const method = Constant.REST_METHOD_DELETE;
  const headers = getHttpHeaders(authenticationToken);
  const response = await fetch(url, { method, headers });

  const responseText = await response.text();
  if (response.status >= 200 && response.status <= 299) {
    if (responseText) {
      return JSON.parse(responseText);
    }
    return undefined;
  }
  throw new Error(responseText);
};

export const saveImageToFile = async (base64Image) => {
  const id = uuid();
  const imageStr = `${Constant.IMAGE_SOURCE_URI_PREFIX}${base64Image}`;
  localStorage.setItem(id, imageStr);
  return { id, content: imageStr };
};

export const getImageFromStorage = imageId => localStorage.getItem(imageId);

const downloadImage = async (url, token) => {
  const image = await sendGetImageRequest(url, token);
  if (image) {
    const img = await saveImageToFile(image);
    return img;
  }
  return undefined;
};

export const removeAllStorage = async () => {
  const keys = _.keys(localStorage);

  keys.forEach((x) => {
    try {
      localStorage.removeItem(x);
    } catch (error) {
      // ignore errors
    }
  });
};

export const registerEmail = (email) => {
  const body = { email };
  return sendPostRequest(Constant.REST_URL_REGISTER, body);
};

export const register = (registrationInfo) => {
  const body = { ...registrationInfo };
  return sendPostRequest(Constant.REST_URL_ACTIVATE_REGISTRATION, body);
};

export const forgetPassword = (email) => {
  const body = { email };
  return sendPostRequest(Constant.REST_URL_FORGET_PASSWORD, body);
};

export const resetPassword = (email, pin, newPassword) => {
  const body = { email, pin, newPassword };
  return sendPostRequest(Constant.REST_URL_RESET_PASSWORD, body);
};

export const login = async (username, password) => {
  const body = { username, password };
  const auth = await sendPostRequest(Constant.REST_URL_LOGIN, body);
  return auth;
};

export const logout = async token => sendPostRequest(Constant.REST_URL_LOGOUT, null, token);


export const downloadMyProfile = async (token) => {
  const profile = await sendGetRequest(Constant.REST_URL_MYPROFILE, token);
  return profile;
};

export const removeProfilePictureFromStorage = async (userId, state) => {
  const user = state.users[userId];
  if (user) {
    const img = state.images[user.profilePicture];
    if (img) {
      localStorage.removeItem(img.id);
    }
  }
};

export const downloadProfilePicture = async (profileId, token) => {
  const url = Constant.REST_URL_PROFILE_PICTURE.replace(/\{id\}/, profileId);
  const img = await downloadImage(url, token);
  return img;
};

export const uploadProfilePicture = async (newProfilePict, profileId, token) => {
  const body = newProfilePict;
  const url = Constant.REST_URL_PROFILE_PICTURE.replace(/\{id\}/, profileId);
  await sendPostImageRequest(url, body, token);
};

export const uploadProfileDetails = async (newProfileDetails, token) => {
  const body = newProfileDetails;
  const url = Constant.REST_URL_MYPROFILE;
  await sendPostRequest(url, body, token);
};

export const updatePassword = async (oldPassword, newPassword, token) => {
  const body = { oldPassword, newPassword };
  const url = Constant.REST_URL_CHANGE_PASSWORD;
  await sendPostRequest(url, body, token);
};

export const deleteProfilePicture = async (profileId, token) => {
  const url = Constant.REST_URL_PROFILE_PICTURE.replace(/\{id\}/, profileId);
  await sendDeleteRequest(url, token);
};

export const renderReduxFormTextField = ({
  input,
  label,
  type,
  required,
  disabled,
  className,
  meta: { touched, error },
}) => (
  <div>
    <FormControl margin="normal" required={required} disabled={disabled} className={className} error={touched && !!error}>
      <InputLabel htmlFor="component-error">{label}</InputLabel>
      <Input type={type} {...input} />
      {touched && error && <FormHelperText>{error}</FormHelperText>}
    </FormControl>
  </div>
);
