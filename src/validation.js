import _ from 'lodash';
import * as Constant from './constant';
import LocalizedString from './localization';


export const validEmail = (email) => {
  // eslint-disable-next-line
    const regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return email ? email.match(regexMail) : false;
};

export const validPhoneNumber = (phoneNumber) => {
  const regexPhone = /^((\+[1-9]{1,2})|[0])[1-9][0-9]{3,10}$/;
  return phoneNumber ? phoneNumber.match(regexPhone) : false;
};

export const validatePasswordComplexity = (password) => {
  if (!password || password.length < Constant.PASSWORD_MIN_LENGTH) {
    throw new Error(LocalizedString.common.errMsgMinPasswordLength);
  }
};

const rxFormValidateRequiredFields = (values, requiredFields) => {
  const errors = {};
  requiredFields.forEach((field) => {
    if (!values[field]) { errors[field] = LocalizedString.common.errMsgRequired; }
  });
  return errors;
};

export const rxformValidateChangePassword = (values) => {
  const requiredFields = [
    Constant.RXFIELD_OLD_PASSWORD,
    Constant.RXFIELD_NEW_PASSWORD,
    Constant.RXFIELD_RE_NEW_PASSWORD,
  ];

  const errors = rxFormValidateRequiredFields(values, requiredFields);

  if (values.newPassword !== values.reNewPassword) {
    errors.reNewPassword = LocalizedString.common.errMsgPasswordDoesNotMatch;
  }

  return errors;
};

export const rxformValidateResetPassword = (values) => {
  const requiredFields = [
    Constant.RXFIELD_PASSWORD,
    Constant.RXFIELD_REPASSWORD,
  ];

  const errors = rxFormValidateRequiredFields(values, requiredFields);

  if (values.password !== values.repassword) {
    errors.repassword = LocalizedString.common.errMsgPasswordDoesNotMatch;
  }

  return errors;
};

export const rxformValidateProfileInfo = (values) => {
  const requiredFields = [
    Constant.RXFIELD_FULLNAME,
    Constant.RXFIELD_PHONE,
    Constant.RXFIELD_JOB_TITLE];

  const errors = rxFormValidateRequiredFields(values, requiredFields);

  if (!validPhoneNumber(values.phone)) {
    errors.phone = LocalizedString.common.errMsgInvalidPhoneNumberFormat;
  }
  return errors;
};

export const rxformValidateRegistrationInfo = (values) => {
  const requiredFields = [
    Constant.RXFIELD_FULLNAME,
    Constant.RXFIELD_PHONE,
    Constant.RXFIELD_JOB_TITLE,
    Constant.RXFIELD_PASSWORD,
    Constant.RXFIELD_REPASSWORD,
  ];

  const errors = rxFormValidateRequiredFields(values, requiredFields);

  if (!validPhoneNumber(values.phone)) {
    errors.phone = LocalizedString.common.errMsgInvalidPhoneNumberFormat;
  }

  if (values.password !== values.repassword) {
    errors.repassword = LocalizedString.common.errMsgPasswordDoesNotMatch;
  }
  return errors;
};

export const validateRegistrationInfo = (registrationInfo) => {
  const error = rxformValidateRegistrationInfo(registrationInfo);
  const values = _.values(error);

  if (values.some(x => x === LocalizedString.common.errMsgRequired)) {
    throw new Error(LocalizedString.common.errMsgEmptyRequiredFields);
  }

  if (values.length > 0) {
    throw new Error(values[0]);
  }
};

export const validateProfileInfo = (profileInfo) => {
  const error = rxformValidateProfileInfo(profileInfo);
  const values = _.values(error);

  if (values.some(x => x === LocalizedString.common.errMsgRequired)) {
    throw new Error(LocalizedString.common.errMsgEmptyRequiredFields);
  }

  if (values.length > 0) {
    throw new Error(values[0]);
  }
};

export const validatePin = (pin) => {
  const matchesPattern = /[0-9]{6}/.test(pin);

  if (pin.length !== Constant.PIN_LENGTH || !matchesPattern) {
    throw new Error(LocalizedString.pinVerificationScreen.errMsgPinMustBeSixDigits);
  }
};
