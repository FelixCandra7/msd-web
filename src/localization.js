import LocalizedStrings from 'react-localization';
import * as Constant from './constant';

const LocalizedString = new LocalizedStrings({
  'en-US': {
    common: {
      alertTitleInfo: 'INFO',
      alertTitleError: 'ERROR',
      alertTitleWarning: 'WARNING',
      alertTitleConfirmation: 'CONFIRMATION',

      buttonCaptionOK: 'OK',
      buttonCaptionCancel: 'CANCEL',
      buttonCaptionDetail: 'DETAIL',
      buttonCaptionYes: 'YES',
      buttonCaptionNo: 'NO',
      buttonCaptionSave: 'SAVE',
      buttonCaptionEdit: 'EDIT',
      buttonCaptionSubmit: 'SUBMIT',
      buttonCaptionNext: 'NEXT',
      buttonCaptionPrev: 'PREV',
      buttonCaptionBack: 'BACK',

      errMsgEmptyRequiredFields: 'Required fields cannot be empty',
      errMsgInvalidEmailFormat: 'The email address is invalid',
      errMsgInvalidPhoneNumberFormat: 'The phone number format is invalid',
      errMsgMinPasswordLength: `Minimum password length is ${Constant.PASSWORD_MIN_LENGTH} characters`,
      errMsgPasswordDoesNotMatch: 'The password does not match',
      errMsgCannotOpenUrl: 'Cannot open the URL',
      errMsgRequired: 'Required',
    },
    loginScreen: {
      title: 'Log In',
      buttonCaptionLogin: 'LOGIN',
      buttonCaptionForgetPassword: 'Forget Password',
      buttonCaptionRegister: 'Register',
      labelUsername: 'Username',
      labelPassword: 'Password',
      errEmptyUsernamePassword: 'Email and password cannot be empty',
    },
    forgetPasswordScreen: {
      title: 'Forget Password',
      lableEmail: 'Email',
      labelNewPassword: 'New Password',
      labelConfirmNewPassword: 'Retype New Password',
      description: 'Write your email here. We will send a PIN to your email for resetting your password',
    },
    registerEmailScreen: {
      title: 'Registration',
      labelEmail: 'Email',
      description: 'Write your email here. We will send a PIN to your email to continue the registration process',
    },
    pinVerificationScreen: {
      title: 'PIN Verification',
      subTitle: 'Please enter PIN code we just sent to your email',
      labelPin: 'PIN',
      expirationLabel: 'Your PIN will expire in',
      errMsgPinMustBeSixDigits: 'The PIN must be six digits',
    },
    registrationInfoScreen: {
      title: 'Registration Info',
      labelFullName: 'Full Name',
      labelPhone: 'Phone',
      labelJobTitle: 'Job Title',
      labelPassword: 'Password',
      labelConfirmPassword: 'Confirm Password',

      buttonCaptionRegister: 'REGISTER',
    },
    homeScreen: {
      title: 'Home',
    },
    profileScreen: {
      title: 'Profile',
      labelFullName: 'Full Name',
      labelEmail: 'Email',
      labelPhoneNumber: 'Phone Number',
      labelJobTitle: 'Job Title',
      labelOldPassword: 'Old Password',
      labelNewPassword: 'New Password',
      labelConfirmNewPassword: 'Retype New Password',

      buttonCaptionChangePassword: 'Change Password',
    },
  },
  in: {
    common: {
      alertTitleInfo: 'INFO',
      alertTitleError: 'GALAT',
      alertTitleWarning: 'PERINGATAN',
      alertTitleConfirmation: 'KONFIRMASI',

      buttonCaptionOK: 'OK',
      buttonCaptionCancel: 'BATAL',
      buttonCaptionDetail: 'DETAIL',
      buttonCaptionYes: 'YA',
      buttonCaptionNo: 'TIDAK',
      buttonCaptionSave: 'SIMPAN',
      buttonCaptionEdit: 'UBAH',
      buttonCaptionSubmit: 'KIRIM',
      buttonCaptionNext: 'LANJUT',
      buttonCaptionPrev: 'SEBELUMNNYA',
      buttonCaptionBack: 'KEMBALI',

      errMsgEmptyRequiredFields: 'Tidak boleh ada field yang kosong',
      errMsgInvalidEmailFormat: 'Format email yang Anda masukkan salah',
      errMsgInvalidPhoneNumberFormat: 'Format nomor telepon yang Anda masukkan salah',
      errMsgMinPasswordLength: `Minimal panjang password adalah ${Constant.PASSWORD_MIN_LENGTH} karakter`,
      errMsgPasswordDoesNotMatch: 'Kedua kata sandi tidak cocok',
      errMsgCannotOpenUrl: 'Tidak bisa membuka URL',
      errMsgRequired: 'Harus diisi',
    },
    loginScreen: {
      title: 'Masuk',
      buttonCaptionLogin: 'MASUK',
      buttonCaptionForgetPassword: 'Lupa Sandi',
      buttonCaptionRegister: 'Daftar',
      labelUsername: 'Nama Pengguna',
      labelPassword: 'Kata Sandi',
      errEmptyUsernamePassword: 'Email dan Sandi tidak boleh kosong',
    },
    forgetPasswordScreen: {
      title: 'Lupa Sandi',
      lableEmail: 'Email',
      labelNewPassword: 'Kata Sandi Baru',
      labelConfirmNewPassword: 'Ulangi Kata Sandi Baru',
      description: 'Tulis email Anda di sini. Kami akan mengirim PIN ke email Anda yang bisa digunakan untuk mengembalikan password Anda',
    },
    registerEmailScreen: {
      title: 'Pendaftaran',
      labelEmail: 'Email',
      description: 'Tulis email Anda di sini. Kami akan mengirim PIN ke email Anda yang bisa digunakan untuk melanjutkan proses registrasi',
    },
    pinVerificationScreen: {
      title: 'Verifikasi PIN',
      subTitle: 'Masukkan PIN yang baru saja kami kirim ke email Anda',
      labelPin: 'PIN',
      expirationLabel: 'PIN akan kadaluarsa dalam',
      errMsgPinMustBeSixDigits: 'PIN harus terdiri dari 6 angka',
    },
    registrationInfoScreen: {
      title: 'Data Pendaftaran',
      labelFullName: 'Nama Lengkap',
      labelPhone: 'Telepon',
      labelJobTitle: 'Jabatan',
      labelPassword: 'Sandi',
      labelConfirmPassword: 'Ulangi Sandi',

      buttonCaptionRegister: 'DAFTAR',
    },
    homeScreen: {
      title: 'Halaman Utama',
    },
    profileScreen: {
      title: 'Profil',
      labelFullName: 'Nama Lengkap',
      labelEmail: 'Email',
      labelPhoneNumber: 'Nomor Telepon',
      labelJobTitle: 'Posisi Jabatan',
      labelOldPassword: 'Kata Sandi Lama',
      labelNewPassword: 'Kata Sandi Baru',
      labelConfirmNewPassword: 'Ulangi Kata Sandi Baru',

      buttonCaptionChangePassword: 'Ubah Sandi',
    },
  },
});

export default LocalizedString;
