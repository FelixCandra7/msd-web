import { connect } from 'react-redux';
import Register from './register.presentation';

const mapStateToProps = state => ({
  errorMsg: state.uiRegistration.error,
});


export default connect(mapStateToProps)(Register);
