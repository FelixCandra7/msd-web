export { default as SplashPage } from './splash';
export { default as LoginPage } from './login';
export { default as HomePage } from './home';
export { default as ProfilePage } from './profile';
export { default as LogoutPage } from './logout';
export { default as ForgetPasswordPage } from './forget-password';
export { default as RegistrationPage } from './register';
