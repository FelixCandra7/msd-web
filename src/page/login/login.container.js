import { connect } from 'react-redux';
import * as Action from '../../redux/action';
import LoginScreen from './login.presentation';
import * as Constant from '../../constant';


const mapStateToProps = state => ({
  loggingIn: state.uiLogin.loggingIn,
  errorMsg: state.uiLogin.error,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onLoginPressed: async ({ username, password }) => {
    try {
      dispatch(Action.clearUIError(Constant.RXSTATE_LOGIN_PAGE));
      await dispatch(Action.loginAsync(username, password, ownProps.history.push));
    } catch (error) {
      dispatch(Action.setUIError(Constant.RXSTATE_LOGIN_PAGE, error.message));
    }
  },
  onForgetPasswordPressed: () => {
  },
  onRegisterPressed: () => {
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
