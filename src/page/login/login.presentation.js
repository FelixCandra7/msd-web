import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { Button, withStyles } from '@material-ui/core';
import * as Constant from '../../constant';
import LocalizedString from '../../localization';
import * as Helper from '../../helper';
import { LoadingIndicator, ErrorMessage } from '../../component';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
});

const LoginScreen = ({
  handleSubmit, onLoginPressed, loggingIn, errorMsg, classes,
}) => (
  <div>
    <h2>{LocalizedString.loginScreen.title}</h2>
    <form onSubmit={handleSubmit(onLoginPressed)}>
      <Field
        name={Constant.RXFIELD_USERNAME}
        type="text"
        component={Helper.renderReduxFormTextField}
        label={LocalizedString.loginScreen.labelUsername}
      />
      <Field
        name={Constant.RXFIELD_PASSWORD}
        type="password"
        component={Helper.renderReduxFormTextField}
        label={LocalizedString.loginScreen.labelPassword}
      />
      <Button variant="contained" color="primary" type="submit" className={classes.button} disabled={loggingIn}>
        {LocalizedString.loginScreen.buttonCaptionLogin}
      </Button>
      <div>
        <Link to={Constant.ROUTE_NAME_FORGET_PASSWORD}>
          {LocalizedString.loginScreen.buttonCaptionForgetPassword}
        </Link>
        <Link to={Constant.ROUTE_NAME_REGISTER_EMAIL}>
          {LocalizedString.loginScreen.buttonCaptionRegister}
        </Link>
      </div>
    </form>
    {loggingIn && <LoadingIndicator />}
    {errorMsg && <ErrorMessage message={errorMsg} />}
  </div>
);

const LoginScreenWithStyles = withStyles(styles)(LoginScreen);

export default reduxForm({
  form: Constant.RXFORM_LOGIN_SCREEN,
})(LoginScreenWithStyles);

LoginScreen.propTypes = {
  classes: PropTypes.object.isRequired,
  loggingIn: PropTypes.bool.isRequired,
  errorMsg: PropTypes.string,
  handleSubmit: PropTypes.func.isRequired,
  onLoginPressed: PropTypes.func.isRequired,
};

LoginScreen.defaultProps = {
  errorMsg: '',
};
