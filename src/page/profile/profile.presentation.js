import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  withStyles, Menu, MenuItem,
} from '@material-ui/core';
import { Link, Route } from 'react-router-dom';
import LocalizedString from '../../localization';
import { ProfileShape } from '../../type';
import * as Constant from '../../constant';
import {
  MainMenu, CurrentProfilePicture, ErrorMessage, LoadingIndicator,
} from '../../component';
import ProfileInfo from './profile-info.container';
import ProfilePictureDialog from './profile-picture-dialog';
import ChangePassword from './change-password.container';

const styles = {
  avatar: {
    margin: 10,
    width: 90,
    height: 90,
  },
};

class ProfileScreen extends Component {
  state = {
    anchorEl: null,
    openImageDialog: false,
  };

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
  };

  handleChange = () => {
    this.setState({ anchorEl: null, openImageDialog: true });
  }

  handleRemove = () => {
    this.setState({ anchorEl: null });
    this.props.onRemoveProfilePicture(this.props.profile.id);
  };

  handleDialogClose = () => {

  }

  onSaveProfilePicture = (image) => {
    this.setState({ openImageDialog: false });
    this.props.onSaveProfilePicture(image);
  }

  onCancelProfilePicture = () => {
    this.setState({ openImageDialog: false });
  }

  render() {
    const { classes, errorMsg, uploadingProfilePicture } = this.props;
    const { anchorEl } = this.state;
    return (
      <div>
        <MainMenu />
        <h2>{LocalizedString.profileScreen.title}</h2>
        <div>
          <CurrentProfilePicture onClick={this.handleClick} className={classes.avatar} />
        </div>
        <div>
          <Route
            path={Constant.ROUTE_NAME_PROFILE}
            exact
            render={() => (
              <div>
                <ProfileInfo />
                <Link to={Constant.ROUTE_NAME_PROFILE_CHANGE_PASSWORD}>
                  {LocalizedString.profileScreen.buttonCaptionChangePassword}
                </Link>
              </div>
            )}
          />

          <Route path={Constant.ROUTE_NAME_PROFILE_CHANGE_PASSWORD} component={ChangePassword} />
        </div>
        {uploadingProfilePicture && <LoadingIndicator />}
        {errorMsg && <ErrorMessage message={errorMsg} />}

        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleMenuClose}
        >
          <MenuItem onClick={this.handleChange}>Change</MenuItem>
          <MenuItem onClick={this.handleRemove}>Remove</MenuItem>
        </Menu>
        <ProfilePictureDialog
          open={this.state.openImageDialog}
          onSave={this.onSaveProfilePicture}
          onCancel={this.onCancelProfilePicture}
        />
      </div>
    );
  }
}
export default withStyles(styles)(ProfileScreen);

ProfileScreen.propTypes = {
  uploadingProfilePicture: PropTypes.bool.isRequired,
  errorMsg: PropTypes.string,
  profile: ProfileShape.isRequired,
  classes: PropTypes.object.isRequired,
  onRemoveProfilePicture: PropTypes.func.isRequired,
  onSaveProfilePicture: PropTypes.func.isRequired,
};

ProfileScreen.defaultProps = {
  errorMsg: '',
};
