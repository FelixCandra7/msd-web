import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import * as Constant from '../../constant';
import LocalizedString from '../../localization';
import * as Helper from '../../helper';
import * as Validation from '../../validation';
import { LoadingIndicator } from '../../component';


const ChangePasswordPage = ({
  handleSubmit, updatingPassword, onUpdatePassword, onCancel,
}) => (
  <div>
    <form onSubmit={handleSubmit(onUpdatePassword)}>
      <Field
        name={Constant.RXFIELD_OLD_PASSWORD}
        type="password"
        disabled={updatingPassword}
        component={Helper.renderReduxFormTextField}
        label={LocalizedString.profileScreen.labelOldPassword}
      />
      <Field
        name={Constant.RXFIELD_NEW_PASSWORD}
        type="password"
        disabled={updatingPassword}
        component={Helper.renderReduxFormTextField}
        label={LocalizedString.profileScreen.labelNewPassword}
      />
      <Field
        name={Constant.RXFIELD_RE_NEW_PASSWORD}
        type="password"
        disabled={updatingPassword}
        component={Helper.renderReduxFormTextField}
        label={LocalizedString.profileScreen.labelConfirmNewPassword}
      />
      <Button variant="contained" color="primary" type="submit" disabled={updatingPassword}>
        {LocalizedString.common.buttonCaptionSave}
      </Button>
      <Button disabled={updatingPassword} onClick={onCancel}>
        {LocalizedString.common.buttonCaptionCancel}
      </Button>
    </form>
    {updatingPassword && <LoadingIndicator />}
  </div>
);

export default reduxForm({
  form: Constant.RXFORM_PROFILE_SCREEN_PASSWORD,
  validate: Validation.rxformValidateChangePassword,
})(ChangePasswordPage);

ChangePasswordPage.propTypes = {
  updatingPassword: PropTypes.bool,
  handleSubmit: PropTypes.func.isRequired,
  onUpdatePassword: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

ChangePasswordPage.defaultProps = {
  updatingPassword: false,
};
