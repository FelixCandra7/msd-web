import React, { Component } from 'react';
import { Dialog, DialogTitle, Button } from '@material-ui/core';
import AvatarEditor from 'react-avatar-editor';
import PropTypes from 'prop-types';
import LocalizedString from '../../localization';
import * as Helper from '../../helper';

export default class ProfilePictureDialog extends Component {
    state = {
      image: null,
    }

    onImageChange = (event) => {
      if (event.target.files && event.target.files[0]) {
        const reader = new FileReader();
        reader.onload = (e) => {
          this.setState({ image: e.target.result });
        };
        reader.readAsDataURL(event.target.files[0]);
      }
    }

    onSave = async () => {
      if (this.editor) {
        // This returns a HTMLCanvasElement, it can be made into a data URL or a blob,
        // drawn on another canvas, or added to the DOM.
        // const canvas = this.editor.getImage();

        // If you want the image resized to the canvas size (also a HTMLCanvasElement)
        const canvas = this.editor.getImageScaledToCanvas().toDataURL();
        const response = await fetch(canvas);
        const blob = await response.blob();
        const base64Image = await Helper.convertBlobToBase64(blob);

        this.props.onSave(base64Image);
      }
    }

    setEditorRef = (editor) => { this.editor = editor; }

    render() {
      return (
        <Dialog
          onClose={this.handleDialogClose}
          open={this.props.open}
        >
          <DialogTitle>Select Picture</DialogTitle>
          <div>
            <input type="file" accept="image/*" onChange={this.onImageChange} />
            <AvatarEditor
              ref={this.setEditorRef}
              image={this.state.image}
              width={200}
              height={200}
              borderRadius={100}
              border={50}
              color={[255, 255, 255, 0.6]} // RGBA
              scale={1.2}
              rotate={0}
            />
            <Button variant="contained" color="primary" type="button" onClick={this.onSave}>
              {LocalizedString.common.buttonCaptionSave}
            </Button>
            <Button variant="contained" color="primary" type="button" onClick={this.props.onCancel}>
              {LocalizedString.common.buttonCaptionCancel}
            </Button>
          </div>
        </Dialog>
      );
    }
}

ProfilePictureDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  onSave: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};
