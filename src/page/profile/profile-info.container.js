import { connect } from 'react-redux';
import { reset } from 'redux-form';
import {
  clearUIError, uploadProfileDetailAsync, setUIError,
  setProfileScreenEditMode, uploadingProfileDetails,
} from '../../redux/action';
import * as Constant from '../../constant';
import ProfileInfo from './profile-info.presentation';

const mapStateToProps = (state) => {
  const { currentUser } = state;
  const user = state.users[currentUser.id];
  return ({
    editable: state.uiProfile.editable,
    uploadingProfileDetail: state.uiProfile.uploadingProfileDetail,
    initialValues: {
      id: currentUser.id,
      username: user.username,
      email: currentUser.email,
      fullName: user.fullName,
      phone: currentUser.phone,
      jobTitle: currentUser.jobTitle,
    },
  });
};

const mapDispatchToProps = dispatch => ({
  onSaveButtonPressed: async (profile) => {
    try {
      dispatch(uploadingProfileDetails(true));
      dispatch(clearUIError(Constant.RXSTATE_PROFILE_PAGE));
      await dispatch(uploadProfileDetailAsync(profile));
      dispatch(setProfileScreenEditMode(false));
    } catch (error) {
      dispatch(setUIError(Constant.RXSTATE_PROFILE_PAGE, error.message));
    } finally {
      dispatch(uploadingProfileDetails(false));
    }
  },
  onCancelButtonPressed: () => {
    dispatch(reset(Constant.RXFORM_PROFILE_SCREEN_INFO));
    dispatch(setProfileScreenEditMode(false));
  },
  onEditButtonPressed: () => {
    dispatch(setProfileScreenEditMode(true));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(ProfileInfo);
