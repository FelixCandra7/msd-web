import React, { Component } from 'react';
import {
  Field, reduxForm,
} from 'redux-form';
import PropTypes from 'prop-types';
import { Button, withStyles } from '@material-ui/core';
import LocalizedString from '../../localization';
import * as Constant from '../../constant';
import * as Helper from '../../helper';
import * as Validation from '../../validation';
import { LoadingIndicator } from '../../component';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
});

class ProfileInfo extends Component {
    onSaveButtonPressed = (values) => {
      this.props.onSaveButtonPressed(values);
    }

    onCancelButtonPressed = () => {
      this.props.onCancelButtonPressed();
    }

    onEditButtonPressed = () => {
      this.props.onEditButtonPressed();
    }

    renderButtons = () => {
      if (this.props.editable) {
        return (
          <div>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              className={this.props.classes.button}
              disabled={this.props.uploadingProfileDetail}
              onClick={this.onSaveButtonPressed}
            >
              {LocalizedString.common.buttonCaptionSave}
            </Button>
            <Button
              type="button"
              className={this.props.classes.button}
              disabled={this.props.uploadingProfileDetail}
              onClick={this.onCancelButtonPressed}
            >
              {LocalizedString.common.buttonCaptionCancel}
            </Button>
          </div>
        );
      }
      return (
        <Button
          variant="contained"
          color="primary"
          type="button"
          className={this.props.classes.button}
          disabled={this.props.uploadingProfileDetail}
          onClick={this.onEditButtonPressed}
        >
          {LocalizedString.common.buttonCaptionEdit}
        </Button>
      );
    }

    render() {
      const {
        uploadingProfileDetail, handleSubmit, editable,
      } = this.props;
      return (
        <div>
          <form onSubmit={handleSubmit(this.onSaveButtonPressed)}>
            <Field
              name={Constant.RXFIELD_EMAIL}
              component={Helper.renderReduxFormTextField}
              type="text"
              disabled
              label={LocalizedString.profileScreen.labelEmail}
            />

            <Field
              name={Constant.RXFIELD_FULLNAME}
              component={Helper.renderReduxFormTextField}
              disabled={!editable || uploadingProfileDetail}
              type="text"
              label={LocalizedString.profileScreen.labelFullName}
              required
            />

            <Field
              name={Constant.RXFIELD_PHONE}
              component={Helper.renderReduxFormTextField}
              disabled={!editable || uploadingProfileDetail}
              type="text"
              label={LocalizedString.profileScreen.labelPhoneNumber}
              keyboardType="numeric"
              required
            />

            <Field
              name={Constant.RXFIELD_JOB_TITLE}
              component={Helper.renderReduxFormTextField}
              disabled={!editable || uploadingProfileDetail}
              type="text"
              label={LocalizedString.profileScreen.labelJobTitle}
            />

            {this.renderButtons()}
          </form>
          {uploadingProfileDetail && <LoadingIndicator />}
        </div>
      );
    }
}

const ProfileInfoWithStyls = withStyles(styles)(ProfileInfo);
export default reduxForm({
  form: Constant.RXFORM_PROFILE_SCREEN_INFO,
  validate: Validation.rxformValidateProfileInfo,
})(ProfileInfoWithStyls);

ProfileInfo.propTypes = {
  editable: PropTypes.bool.isRequired,
  uploadingProfileDetail: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onCancelButtonPressed: PropTypes.func.isRequired,
  onSaveButtonPressed: PropTypes.func.isRequired,
  onEditButtonPressed: PropTypes.func.isRequired,
};
