import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class SplashScreen extends Component {
  componentWillMount() {
    this.props.onAppear();
  }

  render() {
    return (
      <div>
        Splash Screen
      </div>
    );
  }
}

SplashScreen.propTypes = {
  onAppear: PropTypes.func.isRequired,
};
