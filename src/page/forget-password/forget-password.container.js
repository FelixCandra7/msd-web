import { connect } from 'react-redux';
import ForgetPassword from './forget-password.presentation';

const mapStateToProps = state => ({
  errorMsg: state.uiForgetPassword.error,
});


export default connect(mapStateToProps)(ForgetPassword);
