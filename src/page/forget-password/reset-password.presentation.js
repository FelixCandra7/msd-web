import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import * as Constant from '../../constant';
import LocalizedString from '../../localization';
import * as Helper from '../../helper';
import * as Validation from '../../validation';
import { LoadingIndicator } from '../../component';


const ChangePasswordPage = ({
  handleSubmit, submitting, onSubmit, onBack,
}) => (
  <div>
    <form onSubmit={handleSubmit(onSubmit)}>
      <Field
        name={Constant.RXFIELD_PASSWORD}
        type="password"
        disabled={submitting}
        component={Helper.renderReduxFormTextField}
        label={LocalizedString.forgetPasswordScreen.labelNewPassword}
      />
      <Field
        name={Constant.RXFIELD_REPASSWORD}
        type="password"
        disabled={submitting}
        component={Helper.renderReduxFormTextField}
        label={LocalizedString.forgetPasswordScreen.labelConfirmNewPassword}
      />
      <Button disabled={submitting} onClick={onBack}>
        {LocalizedString.common.buttonCaptionBack}
      </Button>
      <Button variant="contained" color="primary" type="submit" disabled={submitting}>
        {LocalizedString.common.buttonCaptionSave}
      </Button>
    </form>
    {submitting && <LoadingIndicator />}
  </div>
);

export default reduxForm({
  form: Constant.RXFORM_RESET_PASSWORD_SCREEN,
  validate: Validation.rxformValidateResetPassword,
})(ChangePasswordPage);

ChangePasswordPage.propTypes = {
  submitting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onBack: PropTypes.func.isRequired,
};
