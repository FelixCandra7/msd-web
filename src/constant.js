export const REST_BASE_URL = 'https://pground01.southeastasia.cloudapp.azure.com:8443/rn-baseline/v1';
export const REST_URL_FORGET_PASSWORD = '/password/forget';
export const REST_URL_REGISTER = '/account/register';
export const REST_URL_ACTIVATE_REGISTRATION = '/account/register/activate';
export const REST_URL_RESET_PASSWORD = '/password/reset';
export const REST_URL_CHANGE_PASSWORD = '/password/change';
export const REST_URL_LOGIN = '/login';
export const REST_URL_LOGOUT = '/logout';
export const REST_URL_MYPROFILE = '/myprofile';
export const REST_URL_PROFILE_PICTURE = '/profile/{id}/picture';

export const REST_METHOD_POST = 'POST';
export const REST_METHOD_GET = 'GET';
export const REST_METHOD_DELETE = 'DELETE';

export const IMAGE_SOURCE_URI_PREFIX = 'data:image/png;base64,';

export const PASSWORD_MIN_LENGTH = 6;
export const PIN_LENGTH = 6;

export const KEY_REDUX_STATE = 'redux-state';

export const HTTP_HEADER_VALUE_JSON = 'application/json';

export const ROUTE_NAME_LOGIN = '/login';
export const ROUTE_NAME_LOGOUT = '/logout';
export const ROUTE_NAME_FORGET_PASSWORD = '/forget-password';
export const ROUTE_NAME_REGISTER_EMAIL = '/register';
export const ROUTE_NAME_PIN_VERIFICATION_FORGET_PASSWORD = '/forget-password/pin';
export const ROUTE_NAME_PIN_VERIFICATION_REGISTER_EMAIL = '/register/pin';
export const ROUTE_NAME_REGISTRATION_INFO = '/register/info';
export const ROUTE_NAME_RESET_PASSWORD = '/forget-password/reset';
export const ROUTE_NAME_SPLASH = '/splash';
export const ROUTE_NAME_HOME = '/home';
export const ROUTE_NAME_SETTING = '/setting';
export const ROUTE_NAME_PROFILE = '/profile';
export const ROUTE_NAME_PROFILE_CHANGE_PASSWORD = '/profile/change-password';
export const ROUTE_NAME_NOTIFICATION = '/notification';

export const RXFORM_PROFILE_SCREEN_INFO = 'profileScreenInfo';
export const RXFORM_PROFILE_SCREEN_PASSWORD = 'profileScreenPassword';
export const RXFORM_LOGIN_SCREEN = 'loginScreen';
export const RXFORM_RESET_PASSWORD_SCREEN = 'resetPasswordScreen';
export const RXFORM_REGISTRATION_INFO_SCREEN = 'registrationInfoScreen';

export const RXFIELD_EMAIL = 'email';
export const RXFIELD_FULLNAME = 'fullName';
export const RXFIELD_PHONE = 'phone';
export const RXFIELD_JOB_TITLE = 'jobTitle';
export const RXFIELD_USERNAME = 'username';
export const RXFIELD_PASSWORD = 'password';
export const RXFIELD_REPASSWORD = 'repassword';
export const RXFIELD_OLD_PASSWORD = 'oldPassword';
export const RXFIELD_NEW_PASSWORD = 'newPassword';
export const RXFIELD_RE_NEW_PASSWORD = 'reNewPassword';

export const RXSTATE_LOGIN_PAGE = 'uiLogin';
export const RXSTATE_PROFILE_PAGE = 'uiProfile';
export const RXSTATE_FORGET_PASSWORD_PAGE = 'uiForgetPassword';
export const RXSTATE_REGISTRATION_PAGE = 'uiRegistration';
