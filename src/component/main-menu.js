import React from 'react';
import { Link } from 'react-router-dom';
import * as Constant from '../constant';

export default () => (
  <div>
    <Link to={Constant.ROUTE_NAME_HOME}>Home</Link>
    <Link to={Constant.ROUTE_NAME_PROFILE}>Profile</Link>
    <Link to={Constant.ROUTE_NAME_LOGOUT}>Logout</Link>
  </div>
);
