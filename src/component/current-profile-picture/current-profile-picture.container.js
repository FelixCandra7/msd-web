import { connect } from 'react-redux';
import CurrentProfilePicture from './current-profile-picture.presentation';
import * as Action from '../../redux/action';

const mapStateToProps = (state) => {
  const { currentUser, users, images } = state;
  const imageId = users[currentUser.id].profilePicture;
  return {
    profilePicture: images[imageId] ? images[imageId].content : '',
  };
};

const mapDispatchToProps = dispatch => ({
  onAppear: () => {
    dispatch(Action.loadCurrentUserProfilePicture());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CurrentProfilePicture);
