module.exports = {
    "globals": {
      "fetch": false
    },
    "env": {
      "browser": true,
      "es6": true,
      "node": true,
      "jest": true
    },
    "parser": "babel-eslint",
    "extends": "airbnb",
    "rules": {
      "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
      "react/jsx-props-no-spreading": [2, {
        "html": "enforce",
        "custom": "ignore",
        "explicitSpread": "enforce",
      }],
      "react/destructuring-assignment": [0, 'never'],
      "import/no-extraneous-dependencies": ["error", {"devDependencies": true}],
      "jsx-a11y/label-has-associated-control": [0],
      "jsx-a11y/label-has-for": [0],
      "react/forbid-prop-types": [1, { "forbid": ["any", "array"] }]
    }
  };
  